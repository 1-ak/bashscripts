INC_BRIGHTNESS () {
if (( $ib <= 95 )); then
	xbacklight -inc 5
fi
}

DEC_BRIGHTNESS () {
if (( $ib >=6 )); then
	xbacklight -dec 5
fi
}

SET_BRIGHTNESS () {
	stty echo
	read -p "Type brightness value in percent : " brightness
	stty -echo
	if [ "$brightness" -eq "$brightness" ] 2> /dev/null
	then
	xbacklight -set $brightness
	else
	echo "Type brightness value in numericals."
	sleep 2
	fi
	clear
}
clear
while true; do
	float_brightness=$(xbacklight -get)
	ib=${float_brightness%.*}
if ((0<=$ib && $ib<=5))
    then
    echo -ne "[:                   ](0${ib}%)\r"
elif ((6<=$ib && $ib<=9))
    then
    echo -ne "[::                  ](0${ib}%)\r"
elif ((10<=$ib && $ib<=15))
    then
    echo -ne "[:::                 ](${ib}%)\r"
elif ((16<=$ib && $ib<=20))
    then
    echo -ne "[::::                ](${ib}%)\r"
elif ((21<=$ib && $ib<=25))
    then
    echo -ne "[:::::               ](${ib}%)\r"
elif ((26<=$ib && $ib<=30))
    then
    echo -ne "[::::::              ](${ib}%)\r"
elif ((31<=$ib && $ib<=35))
    then
    echo -ne "[:::::::             ](${ib}%)\r"
elif ((36<=$ib && $ib<=40))
    then
    echo -ne "[::::::::            ](${ib}%)\r"
elif ((41<=$ib && $ib<=45))
    then
    echo -ne "[:::::::::           ](${ib}%)\r"
elif ((46<=$ib && $ib<=50))
    then
    echo -ne "[::::::::::          ](${ib}%)\r"
elif ((51<=$ib && $ib<=55))
    then
    echo -ne "[:::::::::::         ](${ib}%)\r"
elif ((56<=$ib && $ib<=60))
    then
    echo -ne "[::::::::::::        ](${ib}%)\r"
elif ((61<=$ib && $ib<=65))
    then
    echo -ne "[:::::::::::::       ](${ib}%)\r"
elif ((66<=$ib && $ib<=70))
    then
    echo -ne "[::::::::::::::      ](${ib}%)\r"
elif ((71<=$ib && $ib<=75))
    then
    echo -ne "[:::::::::::::::     ](${ib}%)\r"
elif ((76<=$ib && $ib<=80))
    then
    echo -ne "[::::::::::::::::    ](${ib}%)\r"
elif ((81<=$ib && $ib<=85))
    then
    echo -ne "[:::::::::::::::::   ](${ib}%)\r"
elif ((86<=$ib && $ib<=90))
    then
    echo -ne "[::::::::::::::::::  ](${ib}%)\r"
elif ((91<=$ib && $ib<=95))
    then
    echo -ne "[::::::::::::::::::: ](${ib}%)\r"
elif ((96<=$ib && $ib<=100))
    then
    echo -ne "[:::::::::::::::::::](${ib}%)\r"
fi
	stty echo
	read -n 1 -s -r key
	stty -echo
	case $key in
		"q" )
		exit;;
		"j" )
		INC_BRIGHTNESS;;
		"k" )
		DEC_BRIGHTNESS;;
		"s" )
		SET_BRIGHTNESS;;
	esac
done
